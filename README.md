### Diabetes Risk Estimator ###

The application is created to estimate the probability of user to develop diabetes in the next five years. The predictive model acopted is based on an existing model called German Risk Score (http://www.ncbi.nlm.nih.gov/pubmed/17327313), and a penalty model that takes extra risk factors into account. These extra risk factors include: 

1. after the age of 40, a patient’s risk of developing the disease increases by 2% every 5 years. 

2. if the patient also has a BMI of 40 or more, the risk increases by 2% every 3 years instead. 

3. a family history of the disease makes a person 30% more likely to develop it after the age of 50 and if he is also a smoker.

This web application is now running on a EC2 instance, accessible at www.sunny-song-demo.tk