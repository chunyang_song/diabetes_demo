from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from risk.views import home_page
from risk.models import RiskFactors


class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        """
        Test root url navigate to home page
        """
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_can_save_a_POST_request(self):
        """
        Test data in post request have been saved.
        """
        request = HttpRequest()
        request.method = 'POST'
        request.POST['age'] = 40
        request.POST['height'] = 170
        request.POST['weight'] = 60
        request.POST['smoke'] = 'current'
        request.POST['diet'] = ['meat', 'coffee']
        home_page(request)

        self.assertEqual(RiskFactors.objects.count(), 1)
        new_factors = RiskFactors.objects.first()
        self.assertEqual(new_factors.gender, 'female')
        self.assertEqual(new_factors.age, 40)
        self.assertEqual(new_factors.height, 170)
        self.assertEqual(new_factors.weight, 60)
        self.assertEqual(new_factors.smoke, 'current')
        self.assertEqual(new_factors.hypertension, 'no')
        self.assertEqual(new_factors.diet, ['meat', 'coffee'])
        self.assertIsNone(new_factors.family)

    def test_calculate_risk_and_show_result(self):
        """
        Test risk result is calculated and shown.
        """
        request = HttpRequest()
        request.method = 'POST'
        request.POST['age'] = 25
        request.POST['height'] = 150
        request.POST['weight'] = 50

        response = home_page(request)
        expected_result_text = 'The probability of developing diabetes in the next five years is 0.32%'
        self.assertIn(expected_result_text, response.content.decode())
