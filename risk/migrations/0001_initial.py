# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RiskFactors',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('gender', models.CharField(default='f', choices=[('female', 'Female'), ('male', 'Male')], max_length=6)),
                ('age', models.IntegerField()),
                ('height', models.IntegerField()),
                ('weight', models.IntegerField()),
                ('waist', models.IntegerField(null=True)),
                ('hip', models.IntegerField(null=True)),
                ('smoke', models.CharField(default='no', choices=[('no', 'No'), ('former', 'Former smoker (>20cig./day)'), ('current', 'Current smoker (>20cig./day)')], max_length=13)),
                ('hypertension', models.CharField(default='no', choices=[('no', 'No'), ('yes', 'Yes')], max_length=3)),
                ('diet', multiselectfield.db.fields.MultiSelectField(choices=[('meat', 'Red meat (>150g/day'), ('bread', 'Whole grain bread (>50 g/day)'), ('coffee', 'Coffee (>150 g/day)'), ('alcohol', 'Moderate alcohol (10-40 g/day)')], max_length=25)),
                ('family', multiselectfield.db.fields.MultiSelectField(choices=[('mother', 'Mother'), ('father', 'Father'), ('siblings', 'Siblings')], max_length=22)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
