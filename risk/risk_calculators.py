"""
This risk model is the combination of the German Diabetes Risk Score (available at www.dife.de),
and the extra risk source given in the test, named as penalty. 
"""
import math
from collections import defaultdict
from risk.constants import BASE_LINE_HAZARD, RISK_FACTOR_COEFFICIENTS, PENALTY_RULES


def german_risk_score(raw_args):
    """
    The German Diabetes Risk Score model is expressed as the hazard function: 1 - pow(BASE_LINE_HAZARD, math.exp(score))
    The final risk is the sum of german_model_probability and the penalty.
    """
    args = impute_waist_if_necessary(raw_args)
    score = compute_score(args)
    german_model_probability = 1 - pow(BASE_LINE_HAZARD, math.exp(score))
    penalty = compute_penalty(args)
    probability = german_model_probability + penalty
    percentage = round(probability * 100, 2)
    return percentage


def impute_waist_if_necessary(args):
    """
    Function to populate waist value if it is not provided.
    """
    processed_args = dict(args)
    if args['waist'] is None:
        processed_args['waist'] = _estimate_waist(args)
    return processed_args


def compute_score(args):
    """
    Compute score used in the hazard function, where score is the sum of the product of all pairs of
    variable and coefficient.
    """

    def product(v_c_list): return v_c_list[0] * v_c_list[1]

    # Create a combined dict in the format of: {'factor':[variable, coefficient']},
    # or {'factor':[variable]} if such variable doesnot have coefficient defined, e.g. weight
    variable_coefficient_combined = defaultdict(list)
    # Adding the variables to the combined dict
    variable_coefficient_combined.update({key: [value] for key, value in args.items()})
    # Adding the matching coefficients to the combined dict
    for key, value in RISK_FACTOR_COEFFICIENTS.items():
        variable_coefficient_combined[key].append(value)

    return sum([product(pair) for pair in variable_coefficient_combined.values() if len(pair) > 1])


def compute_penalty(args):
    """
    Compute the extra probability added to base risk if certain conditions are met. The penalty rule is defined
    in constant PENALTY_RULES.
    """
    penalty_labels = []

    has_family_history = any([args[key] for key in ['one_parent', 'both_parents', 'siblings']])
    if (args['age'] >= 50) and args['current_smoker'] and has_family_history:
        penalty_labels.append('age50_family_smoke')
    if args['age'] >= 40:
        penalty_labels.append('age40_plus')
    if _get_bmi(args) >= 40:
        penalty_labels.append('bmi40_plus')

    penalty_prob = 0 if len(penalty_labels) == 0 else sum([PENALTY_RULES[label] for label in penalty_labels])
    return penalty_prob


def _get_bmi(args):
    """
    Calculate bmi using height and weight.
    """
    height_in_meter, weight = args['height'] / 100.0, args['weight']
    bmi = float(weight / (pow(height_in_meter, 2)))
    return bmi


def _estimate_waist(args):
    """
    Estimate the waist circumference in cm.
    If hip circumference is given, use formula (refer to http://jnnp.bmj.com/) to calculate.
    Otherwise assume waist is correlated with bmi.
    """

    def get_waist_by_formula():
        gender, height, weight, hip = args['gender'], args['height'], args['weight'], args['hip']
        formula = {
            'male': (-0.6 * height + weight - 0.392 * hip + 137.432) / 0.785,
            'female': (-0.405 * height + weight - 0.836 * hip + 110.924) / 0.325
        }
        return formula[gender]

    def get_waist_by_bmi(bmi, gender):
        """
        Waist circumference exceeding 94 cm in men and 80 cm in women if bmi >= 25.
        """
        factor = {
            'male': 94 / 25.0,
            'female': 80 / 25.0
        }
        return bmi * factor[gender]

    if args['hip']:
        return get_waist_by_formula()
    else:
        return get_waist_by_bmi(_get_bmi(args), args['gender'])

