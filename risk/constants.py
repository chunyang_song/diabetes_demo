##################################################################
# HTML constants

GENDER_CHOICES = (
    ('female', 'Female'),
    ('male', 'Male'),
)

SMOKE_CHOICES = (
    ('no', 'No'),
    ('former', 'Former smoker (>20cig./day)'),
    ('current', 'Current smoker (>20cig./day)'),
)

HYPERTENSION_CHOICES = (
    ('no', 'No'),
    ('yes', 'Yes'),
)

DIET_CHOICES = (
    ('meat', 'Red meat (>150g/day)'),
    ('bread', 'Whole grain bread (>50 g/day)'),
    ('coffee', 'Coffee (>150 g/day)'),
    ('alcohol', 'Moderate alcohol (10-40 g/day)'),
)

FAMILY_CHOICES = (
    ('mother', 'Mother'),
    ('father', 'Father'),
    ('siblings', 'Siblings'),
)

##################################################################
# German risk score model constants

BASE_LINE_HAZARD = 0.999854

RISK_FACTOR_COEFFICIENTS = {
    'age': 0.05046,
    'height': -0.02399,
    'waist': 0.07621,
    'hypertension': 0.46433,
    'alcohol': -0.17959,
    'former_smoker': 0.3571,
    'current_smoker': 0.65947,
    'bread': -0.08596,
    'coffee': -0.04216,
    'meat': 0.57557,
    'one_parent': 0.55678,
    'both_parents': 1.05717,
    'siblings': 0.47595
}

PENALTY_RULES = {
    'age40_plus': 0.02,  # After the age of 40, + 2% every 5 years.
    'bmi40_plus': 0.02 * (5 / 3.0),  # BMI of 40 or more, + 2% every 3 years, so +(2*(5/3))% every 5 years
    'age50_family_smoke': 0.3  # A family history, age 50, smoker, +30%
}
