from multiselectfield import MultiSelectField
from django.db import models
from risk.constants import *


class RiskFactors(models.Model):
    """
    Defines the type and requirement of each risk factor.
    """
    gender = models.CharField(max_length=6, choices=GENDER_CHOICES, default='f')
    age = models.IntegerField()
    height = models.IntegerField()
    weight = models.IntegerField()
    waist = models.IntegerField(null=True)
    hip = models.IntegerField(null=True)
    smoke = models.CharField(max_length=13, choices=SMOKE_CHOICES, default='no')
    hypertension = models.CharField(max_length=3, choices=HYPERTENSION_CHOICES, default='no')
    diet = MultiSelectField(choices=DIET_CHOICES)
    family = MultiSelectField(choices=FAMILY_CHOICES)


