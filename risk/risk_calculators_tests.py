"""
Unit tests of the functions in risk_calculators module.
"""

import unittest
from risk.risk_calculators import compute_score, compute_penalty, german_risk_score, _estimate_waist


class SpikeFunctionTest(unittest.TestCase):

    def test_compute_score(self):
        args = {
            'age': 29,
            'height': 165,
            'weight': 50,
            'waist': 40,
            'hypertension': 0,
            'alcohol': 0,
            'former_smoker': 0,
            'current_smoker': 1,
            'bread': 1,
            'coffee': 0,
            'read_meat': 1,
            'one_parent': 1,
            'both_parents': 0,
            'siblings': 1
        }
        self.assertEqual(compute_score(args), 2.159629999999999)

    def test_compute_penalty_age40plus(self):
        args = {
            'age': 40,
            'height': 165,
            'weight': 50,
            'waist': 40,
            'hypertension': 0,
            'alcohol': 0,
            'former_smoker': 0,
            'current_smoker': 1,
            'bread': 1,
            'coffee': 0,
            'read_meat': 1,
            'one_parent': 1,
            'both_parents': 0,
            'siblings': 1
        }
        self.assertEqual(compute_penalty(args), 0.02)

    def test_compute_pentaly_age50_family_smoke(self):
        args = {
            'age': 50,
            'height': 165,
            'weight': 50,
            'waist': 40,
            'hypertension': 0,
            'alcohol': 0,
            'former_smoker': 0,
            'current_smoker': 1,
            'bread': 1,
            'coffee': 0,
            'read_meat': 1,
            'one_parent': 1,
            'both_parents': 0,
            'siblings': 1
        }
        self.assertEqual(compute_penalty(args), 0.32)

    def test_compute_penty_bmi_40_plus(self):
        args = {
            'age': 30,
            'height': 150,
            'weight': 100,
            'waist': 96,
            'hypertension': 0,
            'alcohol': 0,
            'former_smoker': 0,
            'current_smoker': 1,
            'bread': 1,
            'coffee': 1,
            'read_meat': 1,
            'one_parent': 1,
            'both_parents': 0,
            'siblings': 0
        }
        self.assertEqual(compute_penalty(args), 0.03333333333333333)

    def test_calculate_risk(self):
        args = {
            'age': 50,
            'height': 150,
            'weight': 100,
            'waist': 96,
            'hypertension': 0,
            'alcohol': 0,
            'former_smoker': 0,
            'current_smoker': 1,
            'bread': 1,
            'coffee': 1,
            'read_meat': 1,
            'one_parent': 1,
            'both_parents': 0,
            'siblings': 0
        }
        self.assertEqual(german_risk_score(args), 55.28)

    def test_estimate_waist_with_hip(self):
        args_female = {
            'gender': 'female',
            'height': 150,
            'weight': 60,
            'hip': 100
        }
        self.assertEqual(_estimate_waist(args_female), 81.76615384615388)

        args_male = {
            'gender': 'male',
            'height': 180,
            'weight': 70,
            'hip': 85
        }
        self.assertEqual(_estimate_waist(args_male), 84.21910828025477)

    def test_estimate_waist_without_hip(self):
        args_female = {
            'gender': 'female',
            'height': 150,
            'weight': 60,
            'hip': None
        }
        self.assertEqual(_estimate_waist(args_female), 85.33333333333334)

        args_male = {
            'gender': 'male',
            'height': 180,
            'weight': 70,
            'hip': None
        }
        self.assertEqual(_estimate_waist(args_male), 81.23456790123456)

if __name__ == '__main__':
    unittest.main()
