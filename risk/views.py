from django.shortcuts import render
from risk.models import RiskFactors
from risk.forms import RiskFactorsForm
from risk.risk_calculators import german_risk_score


def home_page(request):
    """
    Function that handles the get and post request at home page.
    :param request: http request
    :return: http response
    """
    result_text = ''
    if request.method == 'POST':
        form = RiskFactorsForm(data=request.POST)
        if form.is_valid():
            save_data = _set_missing_value_to_default(form.save(commit=False))
            save_data.save()
            args = _prepare_args(save_data)
            risk = german_risk_score(args)
            result_text = 'The probability of developing diabetes in the next five years is {}%'.format(risk)
    else:
        form = RiskFactorsForm()

    return render(request, 'home.html', {'form': form,
                                         'result_text': result_text})


def data_page(request):
    """
    Present the data in the database.
    """
    risk_factors = RiskFactors.objects.all()
    return render(request, 'data_table.html', {'risk_factors': risk_factors})


def _set_missing_value_to_default(post_data):
    """
    Function that set the non-given values to default. Users may not assign the gender, smoke and hypertension fields
    because they appear with their default value on the html form, but not actually set with defaults.
    :param post_data: data contained in the post request
    :return: post_data treated by adding default values to missing fields
    """
    if post_data.gender == '':
        post_data.gender = 'female'
    if post_data.smoke == '':
        post_data.smoke = 'no'
    if post_data.hypertension == '':
        post_data.hypertension = 'no'
    return post_data


def _prepare_args(saved_data):
    """
    Interpret saved_data and prepare argument dict to calculate risk score with.
    :param saved_data: treated post data
    :return: argument dict
    """
    diet = [] if saved_data.diet is None else saved_data.diet
    family = [] if saved_data.family is None else saved_data.family
    return {
        'gender': saved_data.gender,
        'age': saved_data.age,
        'height': saved_data.height,
        'weight': saved_data.weight,
        'waist': saved_data.waist,
        'hip': saved_data.hip,
        'hypertension': 1 if saved_data.hypertension == 'yes' else 0,
        'alcohol': 1 if 'alcohol' in diet else 0,
        'former_smoker': 1 if saved_data.smoke == 'former' else 0,
        'current_smoker': 1 if saved_data.smoke == 'current' else 0,
        'bread': 1 if 'bread' in diet else 0,
        'coffee': 1 if 'coffee' in diet else 0,
        'meat': 1 if 'meat' in diet else 0,
        'one_parent': 1 if ('mother' in family) != ('father' in family) else 0,
        'both_parents': 1 if 'mother' in family and 'father' in family else 0,
        'siblings': 1 if 'siblings' in family else 0,
    }
