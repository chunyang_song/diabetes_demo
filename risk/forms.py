from django import forms
from risk.models import RiskFactors


class RiskFactorsForm(forms.ModelForm):
    """
    Defines the form shown on the home page.
    """
    def __init__(self, *args, **kwargs):
        super(RiskFactorsForm, self).__init__(*args, **kwargs)
        for field in ['gender', 'waist', 'hip', 'smoke', 'hypertension', 'diet', 'family']:
            self.fields[field].required = False

    class Meta:
        """
        This form uses all the fields defined in the RiskFactors class.
        Make the diet and family checkboxes to allow multiple selections.
        """
        model = RiskFactors
        fields = '__all__'
        widgets = {
            'diet': forms.CheckboxSelectMultiple(),
            'family': forms.CheckboxSelectMultiple()
        }

        labels = {
            'height': 'Height (cm)',
            'weight': 'Weight (kg)',
            'waist': 'Waist Circumference (cm)',
            'hip': 'Hip circumference (cm)',
            'smoke': 'Do you smoke?',
            'hypertension': 'Do you have hypertension?',
            'diet': 'Is any item below part of your diet?',
            'family': 'Any family member have been diagnosed Diabetes?',
        }



