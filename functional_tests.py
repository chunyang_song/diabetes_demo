from selenium import webdriver
import unittest


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        """
        Open up browser.
        """
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        """
        Turn off browser.
        """
        self.browser.quit()

    def test_calculate_risk(self):
        """
        End-to-end flow.
        """
        self.browser.get('http://localhost:8000')

        # Check header
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Diabetes Risk Estimator', header_text)

        # Enter personal information
        age_input = self.browser.find_element_by_id('id_age')
        height_input = self.browser.find_element_by_id('id_height')
        weight_input = self.browser.find_element_by_id('id_weight')
        submit_button = self.browser.find_element_by_id('id_submit_button')

        age_input.send_keys(40)
        height_input.send_keys(170)
        weight_input.send_keys(60)
        submit_button.click()

        result_text = self.browser.find_element_by_id('id_result')
        expected_text = "The probability of developing diabetes in the next five years is 2.29%"
        self.assertEqual(result_text.text, expected_text)

if __name__ == '__main__':
    unittest.main(warnings='ignore')


